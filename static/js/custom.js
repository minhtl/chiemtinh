var svgns = "http://www.w3.org/2000/svg";
var svg = null;
var svgWidth = 1000;
var svgHeight = 1220;
var centerX = svgWidth / 2;
var centerY = 440;
var startAngle;
aspect_colors = [ "red", "#06ba07", "black", "blue", "black", "red", "blue", "black", "black", "#06ba07", "red" ];
// COLOR:
// Red: #cc4b4c rgb(204, 75, 76)
// Green #1abb9c rgb(26, 187, 156)
// Orange: #f0ad4e rgb(240, 173, 78)
// Blue: #337ab7 rgb(51, 122, 183)

// SUN = 'Sun'
// MOON = 'Moon'
// MERCURY = 'Mercury'
// VENUS = 'Venus'
// MARS = 'Mars'
// JUPITER = 'Jupiter'
// SATURN = 'Saturn'
// URANUS = 'Uranus'
// NEPTUNE = 'Neptune'
// PLUTO = 'Pluto'
// CHIRON = 'Chiron'
// NORTH_NODE = 'North Node'

// Aries, Taurus, Gemini, Cancer, Leo, Virgo, Libra, Scorpio, Sagittarius, Capricorn, Aquarius and Pisces
var zodiacList = [
    { 
        name: 'Aries',
        shortName: 'Ari',
        startAngle : null,
        category: { x: 0, y: 0 },
        signPath: '/static/images/signs/sign0.png'
    },
    { 
        name: 'Taurus',
        shortName: 'Tau',
        startAngle : null,
        category: { x: 1, y: 2 },
        signPath: '/static/images/signs/sign1.png'
    },
    { 
        name: 'Gemini',
        shortName: 'Gem',
        startAngle : null,
        category: { x: 2, y: 1 },
        signPath: '/static/images/signs/sign2.png'
    },
    { 
        name: 'Cancer',
        shortName: 'Can',
        startAngle : null,
        category: { x: 0, y: 3 },
        signPath: '/static/images/signs/sign3.png'
    },
    { 
        name: 'Leo',
        shortName: 'Leo',
        startAngle : null,
        category: { x: 1, y: 0 },
        signPath: '/static/images/signs/sign4.png'
    },
    { 
        name: 'Virgo',
        shortName: 'Vir',
        startAngle : null,
        category: { x: 2, y: 2 },
        signPath: '/static/images/signs/sign5.png'
    },
    { 
        name: 'Libra',
        shortName: 'Lib',
        startAngle : null,
        category: { x: 0, y: 1 },
        signPath: '/static/images/signs/sign6.png'
    },
    { 
        name: 'Scorpio',
        shortName: 'Sco',
        startAngle : null,
        category: { x: 1, y: 3 },
        signPath: '/static/images/signs/sign7.png'
    },
    { 
        name: 'Sagittarius',
        shortName: 'Sag',
        startAngle : null,
        category: { x: 2, y: 0 },
        signPath: '/static/images/signs/sign8.png'
    },
    { 
        name: 'Capricorn',
        shortName: 'Cap',
        startAngle : null,
        category: { x: 0, y: 2 },
        signPath: '/static/images/signs/sign9.png'
    },
    { 
        name: 'Aquarius',
        shortName: 'Aqu',
        startAngle : null,
        category: { x: 1, y: 1 },
        signPath: '/static/images/signs/sign10.png'
    },
    { 
        name: 'Pisces',
        shortName: 'Pis',
        startAngle : null,
        category: { x: 2, y: 3 },
        signPath: '/static/images/signs/sign11.png'
    },
]

function drawAstro() {
    svg = document.getElementById("main-svg");
    svgWidth = $("#main-svg")[0].clientWidth;

    // Draw main circle
    drawCircle(centerX, centerY, 420, "rgba(217, 255, 255, 1)", null, null);

    // Draw 12 zodiac areas
    drawZodiacAreas();

    // Draw centered circles
    drawCircle(centerX, centerY, 220, "rgba(255, 255, 240, 1)", "black", 1);
    drawCircle(centerX, centerY, 35, "rgba(255, 255, 240, 1)", "black", 1);
    drawCircle(centerX, centerY, 2, "black", "black", 1);

    // Draw houses
    drawHouses();

    // Draw planets
    drawPlanets();
    adjustOtherPlanets();
    drawProgressPlanets();
    drawTransitPlanets();
    drawOrbs();

    // Draw tables
    drawTable1();
    drawTable2();
    drawTable3();
}

function drawZodiacAreas() {
    // Draw 1st circle
    drawCircle(centerX, centerY, 370, "rgba(255, 255, 240, 1)", "black", 1);

    // Draw 2nd circle
    drawCircle(centerX, centerY, 310, "rgba(240, 255, 255, 1)", "black", 1);

    // Draw indicator lines
    firstAngle = 30 - houses[0].signlon;
    for (let i = 0; i < 12; i++) {
        if (zodiacList[i].name == houses[0].sign) {
            for (let j = 0; j < 12; j++) {
                let angle0 = firstAngle + j * 30;
                zodiacList[(i + j + 1) % 12].startAngle = angle0;
                drawLine(centerX + dx(310, angle0), centerY + dy(310, angle0), centerX + dx(370, angle0), centerY + dy(370, angle0), "black", 1, null);
                for (let x = 0; x < 30; x++)
                {
                    if (x % 5 == 0) {
                        drawLine(centerX + dx(310, angle0 + x), centerY + dy(310, angle0 + x), centerX + dx(302, angle0 + x), centerY + dy(302, angle0 + x), "black", 1, null);
                        drawLine(centerX + dx(370, angle0 + x), centerY + dy(370, angle0 + x), centerX + dx(378, angle0 + x), centerY + dy(378, angle0 + x), "black", 1, null);
                    } else {
                        drawLine(centerX + dx(310, angle0 + x), centerY + dy(310, angle0 + x), centerX + dx(306, angle0 + x), centerY + dy(306, angle0 + x), "black", 1, null);
                        drawLine(centerX + dx(370, angle0 + x), centerY + dy(370, angle0 + x), centerX + dx(374, angle0 + x), centerY + dy(374, angle0 + x), "black", 1, null);
                    }
                }
                drawImage("/static/images/signs/sign" + ((i + j + 1) % 12).toString() + ".png", centerX + dx(340, angle0 + 15), centerY + dy(340, angle0 + 15), 42, 42, center=true);
            }
            break;
        }
    }
}

function drawHouses() {
    for (let i = 0; i < 12; i++) {
        angle = houses[i].startAngle;
        strokeWidth = 1;
        if (i % 3 == 0) {
            strokeWidth = 2;
            let convertAngle = convertAngleMinute(houses[i].signlon);
            if (i == 0) {
                // AC
                drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "black", strokeWidth, "null");
                drawImage("/static/images/planets/AC.png", centerX + dx(390, angle), centerY + dy(390, angle), 20, 20, center=true);
                drawTextWithAngle(convertAngle[0].toString(), convertAngle[1].toString(), centerX + dx(407, angle), centerY + dy(407, angle), "12px", "9px", "black", "#6a6a6a", "middle", "middle");
            }
            if (i == 3) {
                // IC
                drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "black", strokeWidth, null);
                drawImage("/static/images/planets/IC.png", centerX + dx(390, angle), centerY + dy(390, angle), 20, 20, center=true);
                drawTextWithAngle(convertAngle[0].toString(), convertAngle[1].toString(), centerX + dx(407, angle), centerY + dy(407, angle), "12px", "9px", "black", "#6a6a6a", "middle", "middle");
            }
            if (i == 6) {
                // DC
                drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "black", strokeWidth, null);
                drawImage("/static/images/planets/DC.png", centerX + dx(390, angle), centerY + dy(390, angle), 20, 20, center=true);
                drawTextWithAngle(convertAngle[0].toString(), convertAngle[1].toString(), centerX + dx(407, angle), centerY + dy(407, angle), "12px", "9px", "black", "#6a6a6a", "middle", "middle");
            }
            if (i == 9) {
                // MC
                drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "black", strokeWidth, null);
                drawImage("/static/images/planets/MC.png", centerX + dx(390, angle), centerY + dy(390, angle), 20, 20, center=true);
                drawTextWithAngle(convertAngle[0].toString(), convertAngle[1].toString(), centerX + dx(407, angle), centerY + dy(407, angle), "12px", "9px", "black", "#6a6a6a", "middle", "middle");
            }
        }
        drawLine(centerX + dx(35, angle), centerY + dy(35, angle), centerX + dx(310, angle), centerY + dy(310, angle), "black", strokeWidth, null);
        drawText((i + 1).toString(), centerX + dx(50, angle + houses[i].size / 2), centerY + dy(50, angle + houses[i].size / 2), "13px", "black", "middle", "middle");
    }
}

function drawPlanets() {
    // console.log(houses);
    // Calculate start angle (absolute angle)
    for (let i = 0; i < 12; i++) {
        for (let j = 0; j < 12; j++) {
            if (planets[i].sign == zodiacList[j].name) {
                planets[i].startAngle = zodiacList[j].startAngle + planets[i].signlon;
                if (planets[i].startAngle > 360) {
                    planets[i].startAngle -= 360;
                }
                planets[i].drawAngle = planets[i].startAngle;
                planets[i].drawTextAngle = planets[i].startAngle;
            }
        }
    }
    // If planets are so close, re-calculate draw angle. Minimum distance is 5 degrees
    let mustAdjust = true;
    let count = 0;
    while (mustAdjust && count < 1000) {
        mustAdjust = false;
        // Điều chỉnh khoảng cách vẽ giữa 2 hành tinh
        for (let i = 0; i < 11; i++) {
            for (let j = i + 1; j < 12; j++) {
                if (Math.abs(planets[i].drawAngle - planets[j].drawAngle) < 5) {
                    if (planets[i].drawAngle < planets[j].drawAngle) {
                        planets[i].drawAngle -= 0.1;
                        planets[j].drawAngle += 0.1;
                    } else {
                        planets[i].drawAngle += 0.1;
                        planets[j].drawAngle -= 0.1;
                    }
                    mustAdjust = true;
                }
            }
        }
        // Điều chỉnh khoảng cách vẽ giữa hành tinh và nóc nhà
        for (let i = 0; i < 12; i++) {
            let minAngle = houses[i].startAngle;
            let maxAngle = houses[i].startAngle + houses[i].size;
            for (let j = 0; j < 12; j++) {
                if (planets[j].drawAngle > minAngle && planets[j].drawAngle < maxAngle) {
                    if (planets[j].drawAngle - 3 < minAngle) {
                        planets[j].drawAngle += 0.1;
                    }
                    if (planets[j].drawAngle + 3 > maxAngle) {
                        planets[j].drawAngle -= 0.1;
                    }
                    mustAdjust = true;
                }
            }
        }
        count++;
    }

    mustAdjust = true;
    count = 0;
    while (mustAdjust && count < 1000) {
        mustAdjust = false;
        // Điều chỉnh khoảng cách vẽ TEXT giữa 2 hành tinh
        for (let i = 0; i < 11; i++) {
            for (let j = i + 1; j < 12; j++) {
                if (Math.abs(planets[i].drawTextAngle - planets[j].drawTextAngle) < 6) {
                    if (planets[i].drawTextAngle < planets[j].drawTextAngle) {
                        planets[i].drawTextAngle -= 0.1;
                        planets[j].drawTextAngle += 0.1;
                    } else {
                        planets[i].drawTextAngle += 0.1;
                        planets[j].drawTextAngle -= 0.1;
                    }
                    mustAdjust = true;
                }
            }
        }
        // Điều chỉnh khoảng cách vẽ TEXT giữa hành tinh và nóc nhà
        for (let i = 0; i < 12; i++) {
            let minAngle = houses[i].startAngle;
            let maxAngle = houses[i].startAngle + houses[i].size;
            for (let j = 0; j < 12; j++) {
                if (planets[j].drawTextAngle > minAngle && planets[j].drawTextAngle < maxAngle) {
                    if (planets[j].drawTextAngle - 4 < minAngle) {
                        planets[j].drawTextAngle += 0.1;
                    }
                    if (planets[j].drawTextAngle + 4 > maxAngle) {
                        planets[j].drawTextAngle -= 0.1;
                    }
                    mustAdjust = true;
                }
            }
        }
        count++;
    }

    // Draw planets
    for (let i = 0; i < 12; i++) {
        let angle = planets[i].startAngle;
        let drawAngle = planets[i].drawAngle;
        let drawTextAngle = planets[i].drawTextAngle;
        drawLine(centerX + dx(310, angle), centerY + dy(310, angle), centerX + dx(300, angle), centerY + dy(300, angle), "blue", 2, null);
        drawImage("/static/images/planets/" + planets[i].name + ".png", centerX + dx(285, drawAngle), centerY + dy(285, drawAngle), 30, 30, center=true);
        if (planets[i].isRetrograde) {
            drawText("r", centerX + dx(285, drawAngle) + 12, centerY + dy(285, drawAngle), "11px", "black", "middle", "middle");
        }
        let convertAngle = convertAngleMinute(planets[i].signlon);
        drawTextWithAngle(convertAngle[0].toString(), convertAngle[1].toString(), centerX + dx(255, drawTextAngle), centerY + dy(255, drawTextAngle), "12px", "9px", "black", "#6a6a6a", "middle", "middle");
    }
}

function adjustOtherPlanets() {
    // Calculate start angle (absolute angle)
    for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 12; j++) {
            if (progressPlanets[i].sign == zodiacList[j].name) {
                progressPlanets[i].startAngle = zodiacList[j].startAngle + progressPlanets[i].signlon;
                if (progressPlanets[i].startAngle > 360) {
                    progressPlanets[i].startAngle -= 360;
                }
                progressPlanets[i].drawAngle = progressPlanets[i].startAngle;
                progressPlanets[i].drawTextAngle = progressPlanets[i].startAngle;
            }
        }
    }

    // Calculate start angle (absolute angle)
    for (let i = 5; i < 12; i++) {
        for (let j = 0; j < 12; j++) {
            if (transitPlanets[i].sign == zodiacList[j].name) {
                transitPlanets[i].startAngle = zodiacList[j].startAngle + transitPlanets[i].signlon;
                if (transitPlanets[i].startAngle > 360) {
                    transitPlanets[i].startAngle -= 360;
                }
                transitPlanets[i].drawAngle = transitPlanets[i].startAngle;
                transitPlanets[i].drawTextAngle = transitPlanets[i].startAngle;
            }
        }
    }

    
    // If planets are so close, re-calculate draw angle. Minimum distance is 5 degrees
    let mustAdjust = true;
    let count = 0;
    while (mustAdjust && count < 1000) {
        mustAdjust = false;
        // Điều chỉnh khoảng cách vẽ giữa 2 hành tinh
        for (let i = 0; i < 11; i++) {
            for (let j = i + 1; j < 12; j++) {
                let angle1 = i < 5 ? progressPlanets[i].drawAngle : transitPlanets[i].drawAngle;
                let angle2 = j < 5 ? progressPlanets[j].drawAngle : transitPlanets[j].drawAngle;
                if (Math.abs(angle1 - angle2) < 4) {
                    if (angle1 < angle2) {
                        if (i < 5) {
                            progressPlanets[i].drawAngle -= 0.1;
                        } else {
                            transitPlanets[i].drawAngle -= 0.1;
                        }
                        if (j < 5) {
                            progressPlanets[j].drawAngle += 0.1;
                        } else {
                            transitPlanets[j].drawAngle += 0.1;
                        }
                    } else {
                        if (i < 5) {
                            progressPlanets[i].drawAngle += 0.1;
                        } else {
                            transitPlanets[i].drawAngle += 0.1;
                        }
                        if (j < 5) {
                            progressPlanets[j].drawAngle -= 0.1;
                        } else {
                            transitPlanets[j].drawAngle -= 0.1;
                        }
                    }
                    mustAdjust = true;
                }
            }
        }

        // Điều chỉnh khoảng cách vẽ giữa hành tinh và nóc nhà (do vẽ vòng ngoài nên không cần tính nóc 1 số nhà)
        for (let i = 0; i < 12; i++) {
            let minAngle = houses[i].startAngle;
            let maxAngle = houses[i].startAngle + houses[i].size;
            for (let j = 0; j < 5; j++) {
                if (progressPlanets[j].drawAngle > minAngle && progressPlanets[j].drawAngle < maxAngle) {
                    if (progressPlanets[j].drawAngle - 3 < minAngle && i % 3 == 0) {
                        progressPlanets[j].drawAngle += 0.1;
                        mustAdjust = true;
                    }
                    if (progressPlanets[j].drawAngle + 3 > maxAngle && i % 3 == 2) {
                        progressPlanets[j].drawAngle -= 0.1;
                        mustAdjust = true;
                    }
                }
            }
        }

        // Điều chỉnh khoảng cách vẽ giữa hành tinh và nóc nhà (do vẽ vòng ngoài nên không cần tính nóc 1 số nhà)
        for (let i = 0; i < 12; i++) {
            let minAngle = houses[i].startAngle;
            let maxAngle = houses[i].startAngle + houses[i].size;
            for (let j = 5; j < 12; j++) {
                if (transitPlanets[j].drawAngle > minAngle && transitPlanets[j].drawAngle < maxAngle) {
                    if (transitPlanets[j].drawAngle - 3 < minAngle && i % 3 == 0) {
                        transitPlanets[j].drawAngle += 0.1;
                        mustAdjust = true;
                    }
                    if (transitPlanets[j].drawAngle + 3 > maxAngle && i % 3 == 2) {
                        transitPlanets[j].drawAngle -= 0.1;
                        mustAdjust = true;
                    }
                }
            }
        }
        count++;
    }
}
function drawProgressPlanets() {
    // console.log("Houses: ", houses);
    // console.log("Planets: ", planets);
    // console.log("Progress Houses: ", progressHouses);
    // console.log("Progress Planets: ", progressPlanets);
    // Draw AC + MC
    // drawLine(centerX + dx(310, timeDelta), centerY + dy(310, timeDelta), centerX + dx(320, timeDelta), centerY + dy(320, timeDelta), "#06ba07", strokeWidth, "null");
    // drawText("AC", centerX + dx(335, timeDelta), centerY + dy(335, timeDelta), "13px", "#06ba07", "middle", "middle");

    // drawLine(centerX + dx(310, houses[9].startAngle + timeDelta), centerY + dy(310, houses[9].startAngle + timeDelta), centerX + dx(320, houses[9].startAngle + timeDelta), centerY + dy(320, houses[9].startAngle + timeDelta), "#06ba07", strokeWidth, "null");
    // drawText("MC", centerX + dx(335, houses[9].startAngle + timeDelta), centerY + dy(335, houses[9].startAngle + timeDelta), "13px", "#06ba07", "middle", "middle");

    // Draw planets
    for (let i = 0; i < 5; i++) {
        let angle = progressPlanets[i].startAngle;
        let drawAngle = progressPlanets[i].drawAngle;
        drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "#06ba07", 2, null);
        drawImage("/static/images/planets/" + progressPlanets[i].name + "2.png", centerX + dx(393, drawAngle), centerY + dy(393, drawAngle), 25, 25, center=true);
        if (progressPlanets[i].isRetrograde) {
            drawText("r", centerX + dx(393, drawAngle) + 12, centerY + dy(393, drawAngle), "11px", "#06ba07", "middle", "middle");
        }
    }
}

function drawTransitPlanets() {
    // Draw planets
    for (let i = 5; i < 12; i++) {
        let angle = transitPlanets[i].startAngle;
        let drawAngle = transitPlanets[i].drawAngle;
        drawLine(centerX + dx(370, angle), centerY + dy(370, angle), centerX + dx(380, angle), centerY + dy(380, angle), "red", 2, null);
        drawImage("/static/images/planets/" + transitPlanets[i].name + "0.png", centerX + dx(393, drawAngle), centerY + dy(393, drawAngle), 25, 25, center=true);
        if (transitPlanets[i].isRetrograde) {
            drawText("r", centerX + dx(393, drawAngle) + 12, centerY + dy(393, drawAngle), "11px", "red", "middle", "middle");
        }
    }
}

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees - 180) * Math.PI / 180.0;
    return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
    };
}

function describeArc(x, y, radius, startAngle, endAngle){
    var start = polarToCartesian(x, y, radius, endAngle);
    var end = polarToCartesian(x, y, radius, startAngle);
    var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";
    var d = [
        "M", start.x, start.y, 
        "A", radius, radius, 0, arcSweep, 0, end.x, end.y,
    ].join(" ");
    return d;       
}

function drawArc(cx, cy, r, minAngle, maxAngle, strokeColor, strokeWidth) {
    var path = document.createElementNS(svgns, "path");
    while (minAngle >= 360) {
        minAngle -= 360;
    }
    while (maxAngle >= 360) {
        maxAngle -= 360;
    }
    var arc = describeArc(cx, cy, r, 360 - maxAngle, 360 - minAngle);
    path.setAttributeNS(null, "d", arc);
    path.setAttributeNS(null, "stroke", strokeColor);
    path.setAttributeNS(null, "fill", "none");
    path.setAttributeNS(null, "stroke-width", strokeWidth);
    svg.appendChild(path);
}

function drawTransitOrbs(planet, planetName) {
    let minAngle = zodiacList[zodiacIndex(planet.minPos.sign)].startAngle + planet.minPos.signlon;
    let maxAngle = zodiacList[zodiacIndex(planet.maxPos.sign)].startAngle + planet.maxPos.signlon;
    let currentAngle = zodiacList[zodiacIndex(planet.current.sign)].startAngle + planet.current.signlon;
    let nextYearAngle = zodiacList[zodiacIndex(planet.nextYear.sign)].startAngle + planet.nextYear.signlon;
    drawArc(centerX, centerY, 55 + 265 + planet.layer * 10, minAngle, maxAngle, "red", 1);
    drawLine(centerX + dx(55 + 265 + planet.layer * 10, currentAngle), centerY + dy(55 + 265 + planet.layer * 10, currentAngle), centerX + dx(55 + 268 + planet.layer * 10, currentAngle), centerY + dy(55 + 268 + planet.layer * 10, currentAngle), "red", 1, null);
    drawLine(centerX + dx(55 + 265 + planet.layer * 10, nextYearAngle), centerY + dy(55 + 265 + planet.layer * 10, nextYearAngle), centerX + dx(55 + 268 + planet.layer * 10, nextYearAngle), centerY + dy(55 + 268 + planet.layer * 10, nextYearAngle), "red", 1, null);
    drawImage("/static/images/planets/" + planetName + "0.png", centerX + dx(55 + 265 + planet.layer * 10, minAngle - 1.5), centerY + dy(55 + 265 + planet.layer * 10, minAngle - 1.5), 12, 12, center=true);
}

function drawProgressOrbs() {
    let currentAngle = progressPlanets[1].startAngle;
    let nextYearAngle = zodiacList[zodiacIndex(orbs['Moon'].sign)].startAngle + orbs['Moon'].signlon;
    drawArc(centerX, centerY, 378, currentAngle, nextYearAngle, "#06ba07", 1);
}

function drawOrbs() {
    drawTransitOrbs(orbs['Jupiter'], 'Jupiter');
    drawTransitOrbs(orbs['Saturn'], 'Saturn');
    drawTransitOrbs(orbs['Uranus'], 'Uranus');
    drawTransitOrbs(orbs['Neptune'], 'Neptune');
    drawTransitOrbs(orbs['Pluto'], 'Pluto');
    drawTransitOrbs(orbs['Chiron'], 'Chiron');
    drawProgressOrbs();
}

function drawTable1() {
    drawRect(70 + 20, 145 + 720, 300, 350, "rgba(255, 255, 240, 1)", "darkgray", 1);
    for (let i = 0; i < 12; i++) {
        drawImage("/static/images/planets/" + planets[i].name + ".png", 70 + 20 + 14, 145 + 720 + i * 25 + 12, 20, 20, center=true);
        drawText(displayPlanetName(planets[i].name), 70 + 20 + 30, 145 + 720 + i * 25 + 16, "10px", "black", null, null);
    }
    // Natal
    drawText("Natal", 70 + 120, 145 + 710, "12px", "blue", null, null);
    for (let i = 0; i < 12; i++) {
        let convertAngle = convertAngleMinuteSecond(planets[i].signlon);
        let str = convertAngle[0].toString() + "° " + zodiacShortName(planets[i].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"";
        if (planets[i].isRetrograde) {
            str += "r";
        }
        drawText(str, 70 + 110, 145 + 720 + i * 25 + 16, "10px", "blue", null, null);
    }

    // Progressions
    drawText("Progr.", 70 + 200, 145 + 710, "12px", "#06ba07", null, null);
    for (let i = 0; i < 5; i++) {
        let convertAngle = convertAngleMinute(progressPlanets[i].signlon);
        drawText(convertAngle[0].toString() + "°", 70 + 190, 145 + 720 + i * 25 + 16, "10px", "#06ba07", null, null);
        drawImage("/static/images/signs/sign" + zodiacIndex(progressPlanets[i].sign).toString() + "p.png", 70 + 190 + 25, 145 + 720 + i * 25 + 12, 15, 15, center=true);
        if (progressPlanets[i].isRetrograde) {
            drawText(convertAngle[1].toString() + "'r", 70 + 190 + 25 + 15, 145 + 720 + i * 25 + 16, "10px", "#06ba07", null, null);
        } else {
            drawText(convertAngle[1].toString() + "'", 70 + 190 + 25 + 15, 145 + 720 + i * 25 + 16, "10px", "#06ba07", null, null);
        }
        
    }

    // Transits
    drawText("Transits", 70 + 260, 145 + 710, "12px", "red", null, null);
    for (let i = 5; i < 12; i++) {
        let convertAngle = convertAngleMinute(transitPlanets[i].signlon);
        drawText(convertAngle[0].toString() + "°", 70 + 250, 145 + 720 + i * 25 + 16, "10px", "red", null, null);
        drawImage("/static/images/signs/sign" + zodiacIndex(transitPlanets[i].sign).toString() + "t.png", 70 + 250 + 25, 145 + 720 + i * 25 + 12, 15, 15, center=true);
        if (transitPlanets[i].isRetrograde) {
            drawText(convertAngle[1].toString() + "'r", 70 + 250 + 25 + 15, 145 + 720 + i * 25 + 16, "10px", "red", null, null);
        } else {
            drawText(convertAngle[1].toString() + "'", 70 + 250 + 25 + 15, 145 + 720 + i * 25 + 16, "10px", "red", null, null);
        }
    }
    // AC-2-3-MC-11-12
    drawRect(70 + 20, 145 + 1020, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // AC
    drawRect(70 + 20, 145 + 1045, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // MC
    drawRect(70 + 120, 145 + 1020, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // 2
    drawRect(70 + 120, 145 + 1045, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // 11
    drawRect(70 + 220, 145 + 1020, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // 3
    drawRect(70 + 220, 145 + 1045, 100, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // 12
    drawImage("/static/images/planets/AC.png", 70 + 34, 145 + 1033, 20, 20, center=true);
    let convertAngle = convertAngleMinuteSecond(houses[0].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[0].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 25 + 22, 145 + 1036, "10px", "blue", null, null);
    drawImage("/static/images/planets/MC.png", 70 + 34, 145 + 1057, 20, 20, center=true);
    convertAngle = convertAngleMinuteSecond(houses[9].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[9].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 25 + 22, 145 + 1061, "10px", "blue", null, null);
    drawText("2", 70 + 125, 145 + 1037, "12px", "black", null, null);
    convertAngle = convertAngleMinuteSecond(houses[1].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[1].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 125 + 22, 145 + 1036, "10px", "blue", null, null);
    drawText("11", 70 + 125, 145 + 1062, "12px", "black", null, null);
    convertAngle = convertAngleMinuteSecond(houses[10].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[10].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 125 + 22, 145 + 1061, "10px", "blue", null, null);
    drawText("3", 70 + 225, 145 + 1037, "12px", "black", null, null);
    convertAngle = convertAngleMinuteSecond(houses[2].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[2].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 225 + 22, 145 + 1036, "10px", "blue", null, null);
    drawText("12", 70 + 225, 145 + 1062, "12px", "black", null, null);
    convertAngle = convertAngleMinuteSecond(houses[11].signlon);
    drawText(convertAngle[0].toString() + "° " + zodiacShortName(houses[11].sign) + " " + convertAngle[1].toString() + "' " + convertAngle[2].toString() + "\"", 70 + 225 + 22, 145 + 1061, "10px", "blue", null, null);
}

function zodiacIndex(sign) {
    for (let i = 0; i < 12; i++) {
        if (zodiacList[i].name == sign) {
            return i;
        }
    }
}
function zodiacShortName(sign) {
// Aries, Taurus, Gemini, Cancer, Leo, Virgo, Libra, Scorpio, Sagittarius, Capricorn, Aquarius and Pisces
    return sign.substring(0, 3);
}
function displayPlanetName(name) {
    return name != "North Node" ? name : "True Node";
}
function drawTable2() {
    // Draw at (544, 860), dimension is 4x3 of 82x32
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 4; j++) {
            drawRect(table2X(i) + 1, table2Y(j), 82, 32, "rgba(255, 255, 240, 1)", "darkgray", 1);
        }
    }
    drawText("F", 120 + 524, 741 + 145, "12px", "black", null, null);
    drawText("A", 120 + 524, 773 + 145, "12px", "black", null, null);
    drawText("E", 120 + 524, 805 + 145, "12px", "black", null, null);
    drawText("W", 120 + 524, 837 + 145, "12px", "black", null, null);
    drawText("C", 120 + 582, 710 + 145, "12px", "black", null, null);
    drawText("F", 120 + 663, 710 + 145, "12px", "black", null, null);
    drawText("M", 120 + 744, 710 + 145, "12px", "black", null, null);
    
    // Dem so hanh tinh trong tung sign
    let tmp = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ];
    tmp[zodiacIndex(houses[0].sign)] = 1; // AC
    tmp[zodiacIndex(houses[9].sign)] = 1; // MC
    for (let i = 0; i < 12; i++) {
        tmp[zodiacIndex(planets[i].sign)] += 1;
    }
    // Ve cac hanh tinh, kich thuoc 15x15 (khoang cach tu can giua)
    for (let i = 0; i < 12; i++) {
        if (tmp[i] > 0) {
            let rows = Math.floor((tmp[i] - 1) / 5) + 1;
            let cols = rows > 1 ? 5 : tmp[i];
            let startY = rows == 1 ? 16 : 8;
            let deltaY = 16;
            let deltaX = Math.floor(40 / cols);
            let startX = 2 + deltaX;
            let size = (rows == 1 && cols <= 4) ? 20 : 15;
            k = 0;
            for (let j = 0; j < 12; j++) {
                if (planets[j].sign == zodiacList[i].name) {
                    drawImage("/static/images/planets/" + planets[j].name + zodiacList[i].category.y.toString() + "x.png",
                        table2X(zodiacList[i].category.x) + startX + (k % cols) * deltaX * 2,
                        table2Y(zodiacList[i].category.y) + startY + Math.floor(k / cols) * deltaY,
                        size, size, center=true);
                    k++;
                }
            }
            if (houses[0].sign == zodiacList[i].name) {
                drawImage("/static/images/planets/AC" + zodiacList[i].category.y.toString() + "x.png",
                    table2X(zodiacList[i].category.x) + startX + (k % cols) * deltaX * 2,
                    table2Y(zodiacList[i].category.y) + startY + Math.floor(k / cols) * deltaY,
                    size, size, center=true);
                k++;
            }
            if (houses[9].sign == zodiacList[i].name) {
                drawImage("/static/images/planets/MC" + zodiacList[i].category.y.toString() + "x.png",
                    table2X(zodiacList[i].category.x) + startX + (k % cols) * deltaX * 2,
                    table2Y(zodiacList[i].category.y) + startY + Math.floor(k / cols) * deltaY,
                    size, size, center=true);
                k++;
            }
        }
    }
}

function table2X(i) {
    return 120 + 544 + i * 82;
}

function table2Y(i) {
    return 720 + 145 + i * 32;
}

function drawTable3() {
    for (let i = 0; i < 12; i++) {
        drawImage("/static/images/planets/" + planets[i].name + ".png", table3X(i) + 21, table3Y(i) + 12, 20, 20, center=true);
        for (let j = i + 1; j < 12; j++) {
            drawRect(table3X(i), table3Y(j), 42, 25, "rgba(255, 255, 240, 1)", "darkgray", 1);
        }
    }
    drawImage("/static/images/planets/AC.png", table3X(12) + 21, table3Y(12) + 12, 20, 20, center=true);
    drawImage("/static/images/planets/MC.png", table3X(12) + 21, table3Y(13) + 12, 20, 20, center=true);
    for (let i = 0; i < 12; i++) {
        drawRect(table3X(i), table3Y(12), 42, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // AC
        drawRect(table3X(i), table3Y(13), 42, 25, "rgba(255, 255, 240, 1)", "darkgray", 1); // MC
    }
    
    lines.forEach(l => {
        drawImage("/static/images/aspects/" + (l.k).toString() + ".png", table3X(l.i) + 21, table3Y(l.j) + 12, 20, 20, center=true);
        drawAspectLines(l.i, l.j, l.k);
    });
}

function drawAspectLines(i, j, k) {
    // Only draw lines of first 10 planets
    if (i > 9 || j > 9) {
        return;
    }
    let strokeDashArray = null;
    if (k == 2 || k == 4 || k == 7 || k == 8 || k == 1 || k == 9) {
        strokeDashArray = "3px 6px";
    }
    drawLine(centerX + dx(40 + 170, planets[i].startAngle), centerY + dy(40 + 170, planets[i].startAngle), centerX + dx(40 + 170, planets[j].startAngle), centerY + dy(40 + 170, planets[j].startAngle), aspect_colors[k], 1, strokeDashArray);
    drawLine(centerX + dx(40 + 180, planets[i].startAngle), centerY + dy(40 + 180, planets[i].startAngle), centerX + dx(40 + 171, planets[i].startAngle), centerY + dy(40 + 171, planets[i].startAngle), "blue", strokeWidth, null);
    drawLine(centerX + dx(40 + 180, planets[j].startAngle), centerY + dy(40 + 180, planets[j].startAngle), centerX + dx(40 + 171, planets[j].startAngle), centerY + dy(40 + 171, planets[j].startAngle), "blue", strokeWidth, null);
}

function table3X(i) {
    return 70 + 320 + i * 42;
}

function table3Y(i) {
    return 720 + 145 + i * 25;
}

function drawRect(x, y, width, height, fillColor, strokeColor, strokeWidth) {
    var rect = document.createElementNS(svgns, 'rect');
    rect.setAttributeNS(null, 'x', x);
    rect.setAttributeNS(null, 'y', y);
    rect.setAttributeNS(null, 'height', height);
    rect.setAttributeNS(null, 'width', width);
    rect.setAttributeNS(null, "fill", fillColor);
    if (strokeColor) {
        rect.setAttributeNS(null, "stroke", strokeColor);
        rect.setAttributeNS(null, "stroke-width", strokeWidth);
    }
    svg.appendChild(rect);
}

function drawLine(x1, y1, x2, y2, strokeColor, strokeWidth=1, strokeDashArray) {
    var line = document.createElementNS(svgns, "line");
    line.setAttributeNS(null, "x1", x1);
    line.setAttributeNS(null, "y1", y1);
    line.setAttributeNS(null, "x2", x2);
    line.setAttributeNS(null, "y2", y2);
    line.setAttributeNS(null, "stroke", strokeColor);
    if (strokeDashArray) {
        line.setAttributeNS(null, "stroke-dasharray", strokeDashArray);
        line.setAttributeNS(null, "fill", "none");
    }
    line.setAttributeNS(null, "stroke-width", strokeWidth);
    svg.appendChild(line);
}

function drawCircle(cx, cy, r, fillColor, strokeColor, strokeWidth) {
    var circle = document.createElementNS(svgns, "circle");
    circle.setAttributeNS(null, "cx", cx);
    circle.setAttributeNS(null, "cy", cy);
    circle.setAttributeNS(null, "r",  r);
    circle.setAttributeNS(null, "fill", fillColor);
    if (strokeColor) {
        circle.setAttributeNS(null, "stroke", strokeColor);
        circle.setAttributeNS(null, "stroke-width", strokeWidth);
    }
    svg.appendChild(circle);
}

function drawImage(filename, x, y, width, height, center=false) {
    if (center) {
        x -= width / 2;
        y -= height / 2;
    }
    var image = document.createElementNS(svgns, "image");
    image.setAttributeNS(null,"height", height);
    image.setAttributeNS(null,"width", width);
    image.setAttributeNS(null, "href", filename);
    image.setAttributeNS(null, "x", x);
    image.setAttributeNS(null, "y", y);
    image.setAttributeNS(null, "visibility", "visible");
    svg.appendChild(image);
}

function drawText(content, x, y, fontSize, color, dominantBaseline, textAnchor) {
    var text = document.createElementNS(svgns, "text");
    text.setAttribute("x", x);
    text.setAttribute("y", y);
    text.setAttribute("fill", color);
    text.textContent = content;
    if (fontSize) {
        text.setAttribute("font-size", fontSize);
    }
    if (dominantBaseline) {
        text.setAttribute("dominant-baseline", dominantBaseline);
    }
    if (textAnchor) {
        text.setAttribute("text-anchor", textAnchor);
    }
    svg.appendChild(text);
}

function drawTextWithAngle(content1, content2, x, y, fontSize1, fontsize2, color1, color2, dominantBaseline, textAnchor) {
    var text = document.createElementNS(svgns, "text");
    text.setAttribute("x", x);
    text.setAttribute("y", y);
    text.setAttribute("fill", color1);
    text.setAttribute("font-weight", 600);
    text.textContent = content1;
    if (fontSize1) {
        text.setAttribute("font-size", fontSize1);
    }
    if (dominantBaseline) {
        text.setAttribute("dominant-baseline", dominantBaseline);
    }
    if (textAnchor) {
        text.setAttribute("text-anchor", textAnchor);
    }
    svg.appendChild(text);
    boundary = text.getBoundingClientRect();
    let x2 = x + (boundary.right - boundary.left) / 2 - 3;
    let y2 = y - (boundary.bottom - boundary.top) / 2;
    drawText(content2, x2, y2, fontsize2, color2, null, null);
}

function dx(r0, angle) {
    return -r0 * Math.cos(angle * Math.PI / 180);
}

function dy(r0, angle) {
    return r0 * Math.sin(angle * Math.PI / 180);
}

function convertAngleMinuteSecond(angle) {
    x1 = parseInt(angle);
    x2 = parseInt(((angle - x1) * 60));
    x3 = Math.round((angle - x1 - x2/60) * 60 * 60);
    return [x1, x2, x3];
}

function convertAngleMinute(angle) {
    x1 = parseInt(angle);
    x2 = Math.round(((angle - x1) * 60));
    return [x1, x2];
}