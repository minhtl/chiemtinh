from flask import Flask, render_template
from flask_mysql_connector import MySQL
from flatlib import const, aspects, angle
from flatlib.chart import Chart
from flatlib.datetime import Datetime
from flatlib.geopos import GeoPos
import json
from datetime import datetime, timezone
import math
import base64
from timezonefinder import TimezoneFinder
from datetime import datetime
import pytz

app = Flask(__name__)
app.config['MYSQL_HOST'] = '45.122.221.131'
app.config['MYSQL_USER'] = 'chiemtinh'
app.config['MYSQL_PASSWORD'] = 'Kgp2&Rds1De'
app.config['MYSQL_DB'] = 'astrology'
mysql = MySQL(app)

sign_index = {
   'Aries': 0,
   'Taurus': 1,
   'Gemini': 2,
   'Cancer': 3,
   'Leo': 4,
   'Virgo': 5,
   'Libra': 6,
   'Scorpio': 7,
   'Sagittarius': 8,
   'Capricorn': 9,
   'Aquarius': 10,
   'Pisces': 11
}

signs = ['Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces' ]

hidden_planets = [ 4, 3, 2, 1, 0, 2, 3, 9, 5, 6, 7, 8 ]

details = {}

info = None
# Nguyen Cam Tu
# _time_stamp = 307756800
# _time_zone = 420
# temp_date = datetime.utcfromtimestamp(_time_stamp)

birth_date = None
transit_date = None
progress_date = None
pos = None
chart = None
transit_chart = None
transit_chart1 = None
progress_chart = None
progress_chart1 = None

_transit_date = None
_transit_date1 = None

def calculate_info(id):
   global info
   global birth_date
   global transit_date
   global _transit_date
   global _transit_date1
   global progress_date
   global pos
   global chart
   global transit_chart
   global transit_chart1
   global progress_chart
   global progress_chart1

   sql_query = 'select * from astrology.customers where id = ' + id
   customer = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   _ts = customer.get('birthday').get(0)
   _tz = customer.get('timezone').get(0)

   try:
      # Check DST
      location = customer.get('location').get(0)
      latitude = float(location.split('/')[0])
      longitude = float(location.split('/')[1])
      obj = TimezoneFinder()
      localtime = pytz.timezone(obj.timezone_at(lng=longitude, lat=latitude))

      _bd1 = datetime.utcfromtimestamp(_ts)
      timex = localtime.localize(datetime(_bd1.year, _bd1.month, _bd1.day, _bd1.hour, _bd1.minute))
      xx = int(str(timex)[-5:-3]) * 60 + int(str(timex)[-2:])
      if str(timex)[-6:-5] == '-':
         xx = -xx
      _ts += (xx - _tz) * 60
      _tz = xx
   except Exception as e:
      print(str(e))
      pass

   if _tz > 0:
      c = '+'
   else:
      c = '-'
   _bd_utc = datetime.utcfromtimestamp(_ts - _tz * 60)
   _bd = datetime.utcfromtimestamp(_ts)
   str_customer_name = customer.get('name').get(0).upper()
   str_birth_date = str(_bd.day).zfill(2) + '/' + str(_bd.month).zfill(2) + '/' + str(_bd.year)\
      + ' ' + str(_bd.hour).zfill(2) + ':' + str(_bd.minute).zfill(2)\
      + ' GMT' + c + str(math.trunc(abs(_tz)/60)).zfill(2) + ':' + str(abs(_tz) % 60).zfill(2)

   str_location = customer.get('location').get(0)
   _lat = float(str_location.split('/')[0])
   _lon = float(str_location.split('/')[1])

   addr = customer.get('address').get(0).split(',')
   if len(addr) > 1:
      str_address = addr[0] + ',' + addr[len(addr) - 1]
   else:
      str_address = addr

   _birth_date = datetime(_bd_utc.year, _bd_utc.month, _bd_utc.day, _bd_utc.hour, _bd_utc.minute, 0, 0, tzinfo=timezone.utc)
   # _transit_date = datetime(2020, 11, 24, 0, 0, 0, 0, tzinfo=timezone.utc)
   # _transit_date1 = datetime(2021, 11, 24, 0, 0, 0, 0, tzinfo=timezone.utc)
   _transit_date = datetime(datetime.utcnow().year, datetime.utcnow().month, datetime.utcnow().day, 0, 0, 0, 0, tzinfo=timezone.utc)
   _transit_date1 = datetime(datetime.utcnow().year + 1, datetime.utcnow().month, datetime.utcnow().day, 0, 0, 0, 0, tzinfo=timezone.utc)
   _birth_date_ts = datetime.timestamp(_birth_date)
   
   _transit_date_ts = datetime.timestamp(_transit_date)
   _progress_date_time_delta = (_transit_date_ts - _birth_date_ts) / 31556926
   _progress_date_ts = _birth_date_ts + _progress_date_time_delta * 86400
   _progress_date = datetime.utcfromtimestamp(_progress_date_ts)

   _transit_date_ts1 = datetime.timestamp(_transit_date1)
   _progress_date_time_delta1 = (_transit_date_ts1 - _birth_date_ts) / 31556926
   _progress_date_ts1 = _birth_date_ts + _progress_date_time_delta1 * 86400
   _progress_date1 = datetime.utcfromtimestamp(_progress_date_ts1)

   birth_date = Datetime(str(_birth_date.year) + '/' + str(_birth_date.month) + '/' + str(_birth_date.day), str(_birth_date.hour) + ':' + str(_birth_date.minute), '+00:00')
   transit_date = Datetime(str(_transit_date.year) + '/' + str(_transit_date.month) + '/' + str(_transit_date.day), str(_transit_date.hour) + ':' + str(_transit_date.minute), '+00:00')
   transit_date1 = Datetime(str(_transit_date.year + 1) + '/' + str(_transit_date.month) + '/' + str(_transit_date.day), str(_transit_date.hour) + ':' + str(_transit_date.minute), '+00:00')
   progress_date = Datetime(str(_progress_date.year) + '/' + str(_progress_date.month) + '/' + str(_progress_date.day), str(_progress_date.hour) + ':' + str(_progress_date.minute), '+00:00')
   progress_date1 = Datetime(str(_progress_date1.year) + '/' + str(_progress_date1.month) + '/' + str(_progress_date1.day), str(_progress_date1.hour) + ':' + str(_progress_date1.minute), '+00:00')
   
   pos = GeoPos('14n21', '108e00')
   pos.lat = _lat
   pos.lon = _lon
   chart = Chart(birth_date, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)
   transit_chart = Chart(transit_date, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)
   transit_chart1 = Chart(transit_date1, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)
   progress_chart = Chart(progress_date, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)
   progress_chart1 = Chart(progress_date1, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)

   info = {
      'str_customer_name': str_customer_name,
      'str_birth_date': str_birth_date,
      'str_location': str_address,
      'str_transits_date': str(_transit_date.day).zfill(2) + '/' + str(_transit_date.month).zfill(2) + '/' + str(_transit_date.year),
      'str_progress_date': str(_progress_date.day).zfill(2) + '/' + str(_progress_date.month).zfill(2) + '/' + str(_progress_date.year) + ' ' + str(_progress_date.hour).zfill(2) + ':' + str(_progress_date.minute).zfill(2) + ':' + str(_progress_date.second).zfill(2) + ' UTC'
   }


@app.route('/astrology/<string:input>')
def hello_world(input):
   str_info = base64.b64decode(input).decode()
   id = str_info.split('?')[0]
   timespan_expired = int(str_info.split('=')[1].split('&')[0]) / 1000
   timespan_now = datetime.timestamp(datetime.utcnow())
   if timespan_expired < timespan_now:
      return 'Không có quyền truy cập'
   # id = '137'
   try:
      calculate_info(id)
   except:
      return 'Không đủ thông tin, vui lòng kiểm tra lại: Ngày giờ sinh, nơi sinh,...'
   # Get information of houses
   houses = [ get_house(const.HOUSE1, chart),
      get_house(const.HOUSE2, chart),
      get_house(const.HOUSE3, chart),
      get_house(const.HOUSE4, chart),
      get_house(const.HOUSE5, chart),
      get_house(const.HOUSE6, chart),
      get_house(const.HOUSE7, chart),
      get_house(const.HOUSE8, chart),
      get_house(const.HOUSE9, chart),
      get_house(const.HOUSE10, chart),
      get_house(const.HOUSE11, chart),
      get_house(const.HOUSE12, chart)
   ]
   for i in range(1, 12):
      houses[i]['startAngle'] = houses[i - 1]['startAngle'] + houses[i - 1]['size']
   
   # Get information of planets
   planets = [ get_planet(const.SUN, chart),
      get_planet(const.MOON, chart),
      get_planet(const.MERCURY, chart),
      get_planet(const.VENUS, chart),
      get_planet(const.MARS, chart),
      get_planet(const.JUPITER, chart),
      get_planet(const.SATURN, chart),
      get_planet(const.URANUS, chart),
      get_planet(const.NEPTUNE, chart),
      get_planet(const.PLUTO, chart),
      get_planet(const.NORTH_NODE, chart),
      get_planet(const.CHIRON, chart),
   ]
   # Get lines of aspects
   lines = get_lines(chart)

   # TRANSIT
   # Get information of houses
   transit_houses = [ get_house(const.HOUSE1, transit_chart),
      get_house(const.HOUSE2, transit_chart),
      get_house(const.HOUSE3, transit_chart),
      get_house(const.HOUSE4, transit_chart),
      get_house(const.HOUSE5, transit_chart),
      get_house(const.HOUSE6, transit_chart),
      get_house(const.HOUSE7, transit_chart),
      get_house(const.HOUSE8, transit_chart),
      get_house(const.HOUSE9, transit_chart),
      get_house(const.HOUSE10, transit_chart),
      get_house(const.HOUSE11, transit_chart),
      get_house(const.HOUSE12, transit_chart)
   ]
   for i in range(1, 12):
      transit_houses[i]['startAngle'] = transit_houses[i - 1]['startAngle'] + transit_houses[i - 1]['size']
   
   # Get information of planets
   transit_planets = [ get_planet(const.SUN, transit_chart),
      get_planet(const.MOON, transit_chart),
      get_planet(const.MERCURY, transit_chart),
      get_planet(const.VENUS, transit_chart),
      get_planet(const.MARS, transit_chart),
      get_planet(const.JUPITER, transit_chart),
      get_planet(const.SATURN, transit_chart),
      get_planet(const.URANUS, transit_chart),
      get_planet(const.NEPTUNE, transit_chart),
      get_planet(const.PLUTO, transit_chart),
      get_planet(const.NORTH_NODE, transit_chart),
      get_planet(const.CHIRON, transit_chart),
   ]

   transit_planets1 = [ get_planet(const.SUN, transit_chart1),
      get_planet(const.MOON, transit_chart1),
      get_planet(const.MERCURY, transit_chart1),
      get_planet(const.VENUS, transit_chart1),
      get_planet(const.MARS, transit_chart1),
      get_planet(const.JUPITER, transit_chart1),
      get_planet(const.SATURN, transit_chart1),
      get_planet(const.URANUS, transit_chart1),
      get_planet(const.NEPTUNE, transit_chart1),
      get_planet(const.PLUTO, transit_chart1),
      get_planet(const.NORTH_NODE, transit_chart1),
      get_planet(const.CHIRON, transit_chart1),
   ]

   # PROGRESS
   # Get information of houses
   progress_houses = [ get_house(const.HOUSE1, progress_chart),
      get_house(const.HOUSE2, progress_chart),
      get_house(const.HOUSE3, progress_chart),
      get_house(const.HOUSE4, progress_chart),
      get_house(const.HOUSE5, progress_chart),
      get_house(const.HOUSE6, progress_chart),
      get_house(const.HOUSE7, progress_chart),
      get_house(const.HOUSE8, progress_chart),
      get_house(const.HOUSE9, progress_chart),
      get_house(const.HOUSE10, progress_chart),
      get_house(const.HOUSE11, progress_chart),
      get_house(const.HOUSE12, progress_chart)
   ]
   for i in range(1, 12):
      progress_houses[i]['startAngle'] = progress_houses[i - 1]['startAngle'] + progress_houses[i - 1]['size']
   
   # Get information of planets
   progress_planets = [ get_planet(const.SUN, progress_chart),
      get_planet(const.MOON, progress_chart),
      get_planet(const.MERCURY, progress_chart),
      get_planet(const.VENUS, progress_chart),
      get_planet(const.MARS, progress_chart),
      get_planet(const.JUPITER, progress_chart),
      get_planet(const.SATURN, progress_chart),
      get_planet(const.URANUS, progress_chart),
      get_planet(const.NEPTUNE, progress_chart),
      get_planet(const.PLUTO, progress_chart),
      get_planet(const.NORTH_NODE, progress_chart),
      get_planet(const.CHIRON, progress_chart)
   ]

   # Get details
   get_details(planets, houses)

   # Get orbs (with retrogrades) of 6 planets: Jupiter (5), Saturn (6), Uranus (7), Neptune (8), Pluto (9), Chiron (11) in TRANSIT
   transit_orbs = [
      get_orbs(transit_planets[5], transit_planets1[5], 'Jupiter'),
      get_orbs(transit_planets[6], transit_planets1[6], 'Saturn'),
      get_orbs(transit_planets[7], transit_planets1[7], 'Uranus'),
      get_orbs(transit_planets[8], transit_planets1[8], 'Neptune'),
      get_orbs(transit_planets[9], transit_planets1[9], 'Pluto'),
      get_orbs(transit_planets[11], transit_planets1[11], 'Chiron')
   ]
   max_layer = 0
   k = 0
   while k <= max_layer:
      for i in range(0, 6):
         if transit_orbs[i]['layer'] == k:
            for j in range(i + 1, 6):
               if transit_orbs[i]['layer'] == k and overlay(transit_orbs[i], transit_orbs[j]):
                  transit_orbs[j]['layer'] = k + 1
                  if max_layer < k + 1:
                     max_layer = k + 1
      k = k + 1
   
   orbs = {
      'Moon': get_planet(const.MOON, progress_chart1),
      'Jupiter': transit_orbs[0],
      'Saturn': transit_orbs[1],
      'Uranus': transit_orbs[2],
      'Neptune': transit_orbs[3],
      'Pluto': transit_orbs[4],
      'Chiron': transit_orbs[5]
   }
   return render_template('/index.html', details=details, orbs=orbs, info=info, lines=lines, houses=houses, planets=planets, transitHouses=transit_houses, transitPlanets=transit_planets, progressHouses=progress_houses, progressPlanets=progress_planets)

def overlay(orb1, orb2):
   # adjust min to have space to draw symbol
   min_signlon1 = orb1['minPos']['signlon']
   min_sign_index1 = sign_index[orb1['minPos']['sign']]
   if min_signlon1 >= 3:
      min_signlon1 = min_signlon1 - 3
   else:
      min_signlon1 = 30 + min_signlon1 - 3
      if min_sign_index1 > 0:
         min_sign_index1 = min_sign_index1 - 1
      else:
         min_sign_index1 = 9

   min_signlon2 = orb2['minPos']['signlon']
   min_sign_index2 = sign_index[orb2['minPos']['sign']]
   if min_signlon2 >= 3:
      min_signlon2 = min_signlon2 - 3
   else:
      min_signlon2 = 30 + min_signlon2 - 3
      if min_sign_index2 > 0:
         min_sign_index2 = min_sign_index2 - 1
      else:
         min_sign_index2 = 9

   if greater(min_sign_index1, min_signlon1, min_sign_index2, min_signlon2) and greater(sign_index[orb2['maxPos']['sign']], orb2['maxPos']['signlon'], min_sign_index1, min_signlon1):
      return True
   if greater(min_sign_index2, min_signlon2, min_sign_index1, min_signlon1) and greater(sign_index[orb1['maxPos']['sign']], orb1['maxPos']['signlon'], min_sign_index2, min_signlon2):
      return True
   return False

# Diem 1 phai dung sau diem 2, tinh theo nguoc chieu kim dong ho
def greater(sign1_index, signlon1, sign2_index, signlon2):
   if sign1_index == sign2_index:
      return signlon1 >= signlon2
   if sign1_index == 0:
      return sign2_index >= 9
   if sign2_index == 0:
      return sign1_index <= 3
   if sign1_index == 1:
      return sign2_index >= 9
   if sign2_index == 1:
      return sign1_index <= 9
   return sign1_index > sign2_index

def get_orbs(planet, planet1, planet_name):
   moves = open('static/retrogrades/' + planet_name + '.txt').readlines()
   i1 = 0
   i2 = 999
   for i in range(0, len(moves)):
      d = datetime(int(moves[i][6:10]), int(moves[i][3:5]), int(moves[i][0:2]), 0, 0, 0, 0, tzinfo=timezone.utc)
      if d >= _transit_date:
         i1 = i - 1
         break
   for i in range(0, len(moves)):
      d = datetime(int(moves[i][6:10]), int(moves[i][3:5]), int(moves[i][0:2]), 0, 0, 0, 0, tzinfo=timezone.utc)
      if d >= _transit_date1:
         i2 = i - 1
         break
   current = { 'sign': planet['sign'], 'signlon': planet['signlon'] }
   next_year = { 'sign': planet1['sign'], 'signlon': planet1['signlon'] }
   if greater(sign_index[current['sign']], current['signlon'], sign_index[next_year['sign']], next_year['signlon']):
      min_pos = { 'sign': planet1['sign'], 'signlon': planet1['signlon'] }
      max_pos = { 'sign': planet['sign'], 'signlon': planet['signlon'] }
   else:
      max_pos = { 'sign': planet1['sign'], 'signlon': planet1['signlon'] }
      min_pos = { 'sign': planet['sign'], 'signlon': planet['signlon'] }
   for i in range(i1, i2 + 1):
      # CAN TINH LAI MOC I1, I2
      sl1 = int(moves[i].split('|')[1].split('-')[0])
      si1 = int(moves[i].split('|')[1].split('-')[1])
      sl2 = int(moves[i].split('|')[2].split('-')[0])
      si2 = int(moves[i].split('|')[2].split('-')[1])
      if i != i1 and greater(sign_index[min_pos['sign']], min_pos['signlon'], si1, sl1):
         min_pos['sign'] = signs[si1]
         min_pos['signlon'] = sl1
      if i != i2 and greater(sign_index[min_pos['sign']], min_pos['signlon'], si2, sl2):
         min_pos['sign'] = signs[si2]
         min_pos['signlon'] = sl2
      if i != i1 and greater(si1, sl1, sign_index[max_pos['sign']], max_pos['signlon']):
         max_pos['sign'] = signs[si1]
         max_pos['signlon'] = sl1
      if i != i2 and greater(si2, sl2, sign_index[max_pos['sign']], max_pos['signlon']):
         max_pos['sign'] = signs[si2]
         max_pos['signlon'] = sl2
   # print(min_pos)
   # print(max_pos)
   # print(current)
   # print(next_year)
   return { 'minPos': min_pos, 'maxPos': max_pos, 'current': current, 'nextYear': next_year, 'layer': 0 }



def get_details(planets, houses):
   global details
   ### TINH CACH
   # SUN
   sql_query = 'select * from astrology.contents where page_code = "CT-SUN-' + str(sign_index[planets[0]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'sun': content.get('page_content').get(0) })

   # MOON
   sql_query = 'select * from astrology.contents where page_code = "CT-MOON-' + str(sign_index[planets[1]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'moon': content.get('page_content').get(0) })

   # MERCURY
   sql_query = 'select * from astrology.contents where page_code = "CT-MERCURY-' + str(sign_index[planets[2]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'mercury': content.get('page_content').get(0) })

   # VENUS
   sql_query = 'select * from astrology.contents where page_code = "CT-VENUS-' + str(sign_index[planets[3]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'venus': content.get('page_content').get(0) })

   # MARS
   sql_query = 'select * from astrology.contents where page_code = "CT-MARS-' + str(sign_index[planets[4]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'mars': content.get('page_content').get(0) })

   # JUPITER
   sql_query = 'select * from astrology.contents where page_code = "CT-JUPITER-' + str(sign_index[planets[5]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'jupiter': content.get('page_content').get(0) })

   # SATURN
   sql_query = 'select * from astrology.contents where page_code = "CT-SATURN-' + str(sign_index[planets[6]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'saturn': content.get('page_content').get(0) })

   # NORTH NODE
   sql_query = 'select * from astrology.contents where page_code = "CT-NORTHNODE-' + str(sign_index[planets[10]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   details.update({ 'northnode': content.get('page_content').get(0) })

   ### TỔNG QUAN TẤT CẢ CÁC KHÍA CẠNH TRONG CUỘC ĐỜI
   house_tmps = [ '', '', '', '', '', '', '', '', '', '', '', '' ]
   sql_query = 'select * from astrology.contents where page_code = "CT-AC-' + str(sign_index[houses[0]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   house_tmps[0] = content.get('page_content').get(0)
   for i in range(0, 12):
      already = [ False, False, False, False, False, False, False, False, False, False ]
      # Tính sao ẩn
      sql_query = 'select * from astrology.contents where page_code = "CT-' + str(i + 1) + '-' + str(hidden_planets[sign_index[houses[i]['sign']]] + 1) + '"'
      content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
      house_tmps[i] += content.get('page_content').get(0)
      already[hidden_planets[sign_index[houses[i]['sign']]]] = True
      # Tính sao hiện
      for j in range(0, 10):
         if house_has_planet(houses[i], planets[j]) and not already[j]:
            sql_query = 'select * from astrology.contents where page_code = "CT-' + str(i + 1) + '-' + str(j + 1) + '"'
            content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
            house_tmps[i] += content.get('page_content').get(0)
            already[j] = True
   sql_query = 'select * from astrology.contents where page_code = "CT-THIENDINH-' + str(sign_index[houses[9]['sign']] + 1) + '"'
   content = mysql.execute_sql(sql_query, to_pandas=True).to_dict()
   house_tmps[9] += content.get('page_content').get(0)

   details.update({ 'house_contents': house_tmps })

def house_has_planet(house, planet):
   house_start_angle = sign_index[house['sign']] * 30 + house['signlon']
   house_end_angle = house_start_angle + house['size']
   planet_angle = sign_index[planet['sign']] * 30 + planet['signlon']
   return (planet_angle >= house_start_angle and planet_angle <= house_end_angle) or (planet_angle - 360 >= house_start_angle and planet_angle - 360 <= house_end_angle) or (planet_angle + 360 >= house_start_angle and planet_angle + 360 <= house_end_angle)

def get_lines(chart):
   planets = [
      chart.get(const.SUN),
      chart.get(const.MOON),
      chart.get(const.MERCURY),
      chart.get(const.VENUS),
      chart.get(const.MARS),
      chart.get(const.JUPITER),
      chart.get(const.SATURN),
      chart.get(const.URANUS),
      chart.get(const.NEPTUNE),
      chart.get(const.PLUTO),
      chart.get(const.NORTH_NODE),
      chart.get(const.CHIRON),
      chart.get(const.ASC),
      chart.get(const.MC),
   ]

   aspect_maps = [ 0, 30, 45, 60, 72, 90, 120, 135, 144, 150, 180 ]

   planet_aspect_deltas = [
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # SUN
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # MOON
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # MERCURY
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # VENUS
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # MARS
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # JUPITER
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # SATURN
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # URANUS
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # NEPTUNE
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # PLUTO
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # NORTH_NODE
      [ 10, 3, 3, 6, 2, 10, 10, 3, 2, 3, 10 ], # CHIRON
   ]
   lines = []

   for i in range(0, 12):
      for j in range(i + 1, 14):
         sep = angle.closestdistance(planets[i].lon, planets[j].lon)
         x = abs(sep)
         for k in range(0, 11):
               if abs(x - aspect_maps[k]) <= planet_aspect_deltas[i][k]:
                  lines.append( { 'i': i, 'j': j, 'k': k} )
   return lines

def get_house(name, chart):
   house = chart.get(name)
   return { 'sign': house.sign, 'signlon': house.signlon, 'size': house.size, 'startAngle': 0 }

def get_planet(name, chart):
   planet = chart.getObject(name)
   return { 'name': planet.id, 'sign': planet.sign, 'signlon': planet.signlon, 'isRetrograde': planet.isRetrograde(), 'startAngle': 0, 'drawAngle': 0, 'drawTextAngle': 0, }

if __name__ == '__main__':
   app.run(debug=True, host='0.0.0.0', port='5000')