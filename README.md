# CHIÊM TINH #

### Cài đặt dependencies ###

```
$ sudo apt update && sudo apt upgrade
$ sudo apt install python3
$ sudo apt install python3-pip
$ sudo apt install virtualenv
```

### Tạo thư mục và lấy source về ###

```
$ mkdir thansochiemtinh
$ cd thansochiemtinh
~/thansochiemtinh$ git clone https://minhtl@bitbucket.org/minhtl/chiemtinh.git
~/thansochiemtinh$ cd chiemtinh
```

### Cài đặt virtual environment và kích hoạt ###

```
~/thansochiemtinh/chiemtinh$ virtualenv --python=python3 venv
~/thansochiemtinh/chiemtinh$ source venv/bin/activate
```

### Cài đặt thư viện flatlib ###

```
(venv)~/thansochiemtinh/chiemtinh$ python3 setup.py install
```

Test thử thư viện

```
(venv)~/thansochiemtinh/chiemtinh$ python3 test.py
```

Nếu có lỗi, thực hiện re-install package pyswisspeh

```
(venv)~/thansochiemtinh/chiemtinh$ pip3 uninstall pyswisspeh
(venv)~/thansochiemtinh/chiemtinh$ pip3 install pyswisseph==2.00.00-2
```

### Cài đặt Flask ###

```
(venv)~/thansochiemtinh/chiemtinh$ pip3 install flask
```

### Cài đặt MySQL connector ###

```
(venv)~/thansochiemtinh/chiemtinh$ pip3 install flask-mysql-connector
```

### CHẠY ỨNG DỤNG ###

```
(venv)~/thansochiemtinh/chiemtinh$ python3 main.py
```

URL: *(BASE URL)/astrology/(Customer Id)*