from flatlib import aspects
from flatlib import const, angle
from flatlib.chart import Chart
from flatlib.datetime import Datetime
from flatlib.geopos import GeoPos
from datetime import datetime, timezone, timedelta

# Nguyen Cam Tu
pos = GeoPos('14n21', '108e00')

birth_date = datetime(1985, 4, 19, 1, 50, 0, 0, tzinfo=timezone.utc)
transit_date = datetime(2020, 12, 12, 0, 0, 0, 0, tzinfo=timezone.utc)

birth_date_ts = datetime.timestamp(birth_date)
transit_date_ts = datetime.timestamp(transit_date)

progress_date_time_delta = (transit_date_ts - birth_date_ts) / 31556926
# progress_date_time_delta = 41
progress_date_ts = birth_date_ts + progress_date_time_delta * 86400
progress_date = datetime.utcfromtimestamp(progress_date_ts)

date = Datetime(str(progress_date.year) + '/' + str(progress_date.month) + '/' + str(progress_date.day), str(progress_date.hour) + ':' + str(progress_date.minute) + ':' + str(progress_date.second), '+00:00')
date = Datetime('1985/05/24', '01:52:33', '+00:00')
# date = Datetime(str(_transit_date.year) + '/' + str(_transit_date.month) + '/' + str(_transit_date.day), str(_transit_date.hour) + ':' + str(_transit_date.minute), '+00:00')
# date = Datetime(str(birth_date.year) + '/' + str(birth_date.month) + '/' + str(birth_date.day), str(birth_date.hour) + ':' + str(birth_date.minute), '+00:00')

chart = Chart(date, pos, hsys='Placidus', IDs=const.LIST_OBJECTS)
print(date)
print('')
print('======== HOUSES =========')
print(chart.get(const.HOUSE1))
print(chart.get(const.HOUSE2))
print(chart.get(const.HOUSE3))
print(chart.get(const.HOUSE4))
print(chart.get(const.HOUSE5))
print(chart.get(const.HOUSE6))
print(chart.get(const.HOUSE7))
print(chart.get(const.HOUSE8))
print(chart.get(const.HOUSE9))
print(chart.get(const.HOUSE10))
print(chart.get(const.HOUSE11))
print(chart.get(const.HOUSE12))
print('')
print(chart.get(const.ASC))
print(chart.get(const.MC))
print('')

# Retrieve the Sun and Moon 
planets = [
    chart.get(const.SUN),
    chart.get(const.MOON),
    chart.get(const.MERCURY),
    chart.get(const.VENUS),
    chart.get(const.MARS),
    chart.get(const.JUPITER),
    chart.get(const.SATURN),
    chart.get(const.URANUS),
    chart.get(const.NEPTUNE),
    chart.get(const.PLUTO),
    chart.get(const.NORTH_NODE),
    chart.get(const.CHIRON),
]

# for planet in planets:
#     print(planet)
# major_aspect = aspects.getAspect(planets[0], planets[4], const.MAJOR_ASPECTS)
# for attr in dir(major_aspect):
#     print(attr, getattr(major_aspect, attr))
# x = major_aspect.passive.movement
# for attr in dir(x):
#     print(attr, getattr(x, attr))
# print(x)

sun = chart.get(const.SUN)
moon = chart.get(const.MOON)
mercury = chart.get(const.MERCURY)
venus = chart.get(const.VENUS)
asc = chart.get(const.ASC)
mars = chart.get(const.MARS)
chiron = chart.get(const.CHIRON)
uranus = chart.get(const.URANUS)
jupiter = chart.get(const.JUPITER)
y = jupiter
# y = y.orb
# print(y)
# for attr in dir(y):
#     print(attr, getattr(y, attr))


# ap = aspects._getActivePassive(mars, sun)
# print(ap['active'])
# print(ap['passive'])
# aspDict = aspects._aspectDict(ap['active'], ap['passive'], const.ALL_ASPECTS)
# print(aspDict)


# print(chart.getObject(const.MERCURY))
# h2 = chart.get(const.HOUSE3)
# print(h2.hasObject(m))
# print('======== HAS SUN =========')
# sun = chart.getObject(const.SUN)
# print(chart.get(const.HOUSE1).hasObject(sun))
# print(chart.get(const.HOUSE2).hasObject(sun))
# print(chart.get(const.HOUSE3).hasObject(sun))
# print(chart.get(const.HOUSE4).hasObject(sun))
# print(chart.get(const.HOUSE5).hasObject(sun))
# print(chart.get(const.HOUSE6).hasObject(sun))
# print(chart.get(const.HOUSE7).hasObject(sun))
# print(chart.get(const.HOUSE8).hasObject(sun))
# print(chart.get(const.HOUSE9).hasObject(sun))
# print(chart.get(const.HOUSE10).hasObject(sun))
# print(chart.get(const.HOUSE11).hasObject(sun))
# print(chart.get(const.HOUSE12).hasObject(sun))

